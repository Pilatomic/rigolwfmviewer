#include "traceviewerwidget.h"

#include "wfmdata.h"
#include <QPainter>
#include <QElapsedTimer>
#include <QDebug>

#define TRACES_LEFT_MARGIN      30

#define ORIG_POLY_RIGHT_MARGIN  5
#define ORIG_POLY_TEXT_HEIGHT   10
#define ORIG_POLY_HALF_HEIGHT   6
#define ORIG_POLY_WIDTH         20

#define VERT_ORIG_VAL           0x55
#define VERT_VAL_PER_DIV        16.5
#define VERT_DIV_COUNT          8

#define HORZ_DIV_COUNT          12

const QVector<QColor> TraceViewerWidget::tracesColor = QVector<QColor>(
{
                QColor(0xff, 0xff, 0xff),
                QColor(0xf8, 0xfc, 0x00),
                QColor(0x00, 0xfc, 0xf8),
                QColor(0xf8, 0x00, 0xf8),
                QColor(0x00, 0x7c, 0xf8)
            });

static QPolygon buildChannelZeroPolygon()
{
    QPolygon poly;
    poly << QPoint(-ORIG_POLY_WIDTH, -ORIG_POLY_HALF_HEIGHT)
         << QPoint(-ORIG_POLY_HALF_HEIGHT, -ORIG_POLY_HALF_HEIGHT)
         << QPoint(0, 0)
         << QPoint(-ORIG_POLY_HALF_HEIGHT, ORIG_POLY_HALF_HEIGHT)
         << QPoint(-ORIG_POLY_WIDTH, ORIG_POLY_HALF_HEIGHT);
    return poly;
}

const QPolygon TraceViewerWidget::channelZeroPolygon = buildChannelZeroPolygon();

TraceViewerWidget::TraceViewerWidget(QWidget *parent) : QWidget(parent)
{

}

void TraceViewerWidget::setData(WfmFileDataPtr data)
{
    this->data = data;

    if(data)
    {
        foreach(const WfmChannelData* channel, data->channelsData)
        {
            double sum = 0;
            const qint64 sampleCount = channel->samplesData.count();
            for(int i = 0 ; i < sampleCount ; i++) sum += channel->samplesData.at(i);
            qDebug() << "Average value for channel" << channel->label
                     << ":" << QString::number(sum / sampleCount)
                     << "scale : " << channel->scaleExtended
                     << "offset" << channel->offsetExtended
                     << "rangeID" << channel->rangeId;
//            qDebug() << "Unknown 1:" << QString::fromLatin1(channel->unknownField1.toHex())
//                     << "Unknown 2:" << QString::fromLatin1(channel->unknownField2.toHex())
//                     << "Unknown 3:" << QString::fromLatin1(channel->unknownField3.toHex())
//                     << "Unknown 4:" << QString::fromLatin1(channel->unknownField4.toHex())
//                     << "Unknown 5:" << QString::fromLatin1(channel->unknownField5.toHex());
        }
    }

    update();
}

void TraceViewerWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QElapsedTimer paintElapsedTimer;
    paintElapsedTimer.start();

    QPainter painter(this);

    paintBackGround(&painter);

    if(data)
    {
        foreach(const WfmChannelData * channel, data->getEnabledChannels())
        {
            paintChannelTrace(&painter, channel);
            paintChannelOriginIndicator(&painter, channel);
        }
    }

    qDebug() << "Draw duration :" << paintElapsedTimer.elapsed() << "ms";
}

void TraceViewerWidget::paintChannelTrace(QPainter *painter, const WfmChannelData *channel)
{
    const int traceW = width() - TRACES_LEFT_MARGIN;
    const quint64 samplesCount = channel->samplesData.count();
    const int halfH = height() / 2 ;

    QPoint p0;
    painter->setPen(QPen(tracesColor.at(channel->channelId)));

    for(int x = 0 ; x < traceW ; x++)
    {
        int i = samplesCount * x / traceW;
        int vertDev = int(channel->samplesData.at(i)) - VERT_ORIG_VAL ;
        int y = halfH - (vertDev * height()) / int(VERT_DIV_COUNT*VERT_VAL_PER_DIV) ;
        QPoint p1(x + TRACES_LEFT_MARGIN,y);
        if(x > 0) painter->drawLine(p0,p1);
        p0 = p1;
    }
}

void TraceViewerWidget::paintChannelOriginIndicator(QPainter *painter, const WfmChannelData *channel)
{
    const int origY = height() / 2 - height() * channel->offsetExtended / (channel->scaleExtended * VERT_DIV_COUNT);
    const int channelId = channel->channelId;

    const QPolygon translatedOrigPoly = channelZeroPolygon.translated(TRACES_LEFT_MARGIN - ORIG_POLY_RIGHT_MARGIN, origY);

    painter->setBrush(QBrush(tracesColor.at(channelId)));
    painter->setPen(Qt::black);

    QString chNumStr = QString::number(channelId);

    QFont font = painter->font();
    font.setPixelSize(ORIG_POLY_TEXT_HEIGHT);
    font.setBold(true);
    painter->setFont(font);

    painter->drawPolygon(translatedOrigPoly);
    painter->drawText(translatedOrigPoly.boundingRect(), Qt::AlignHCenter | Qt::AlignVCenter, chNumStr);
}

void TraceViewerWidget::paintBackGround(QPainter *painter)
{

    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::black);
    painter->drawRect(this->rect());

    QPen gridPen(Qt::gray);
    gridPen.setStyle(Qt::DotLine);
    painter->setPen(gridPen);

    const int hm1 = height() - 1;
    const int wm1 = width() - 1;

    // Vertical lines
    for(int i = 1 ; i < HORZ_DIV_COUNT ; i++)
    {
        int x = TRACES_LEFT_MARGIN + (wm1 - TRACES_LEFT_MARGIN) * i / HORZ_DIV_COUNT;
        painter->drawLine(x, 0, x, height());
    }

    // Vertical DIV ( horizontal lines )
    for(int i = 1 ; i < VERT_DIV_COUNT ; i++)
    {
        int y = hm1 * i / VERT_DIV_COUNT;
        painter->drawLine(TRACES_LEFT_MARGIN, y, width(), y);
    }

    //Outside lines
    gridPen.setStyle(Qt::SolidLine);
    painter->setPen(gridPen);
    painter->drawLine(TRACES_LEFT_MARGIN, hm1, wm1, hm1);
    painter->drawLine(wm1, hm1, wm1, 0);
    painter->drawLine(TRACES_LEFT_MARGIN, 0, wm1, 0);
    painter->drawLine(TRACES_LEFT_MARGIN, 0, TRACES_LEFT_MARGIN, hm1);

}

int TraceViewerWidget::mapValueToHeight(int value)
{
    const int h = height();
    return h - (value * h / 256);
}
