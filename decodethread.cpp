#include "decodethread.h"
#include "wfmdecoder.h"
#include "wfmdata.h"

#include <QFile>

DecodeThread::DecodeThread(const QString & path, QObject *parent) :
    QThread(parent), path(path), file(path)
{

}


void DecodeThread::run()
{
    if(!file.open(QFile::ReadOnly))
    {
        emit error(QString("Could not open file %1").arg(path));
        return;
    }

    WfmDecoder decoder(&file);
    connect(&decoder, &WfmDecoder::error, this, &DecodeThread::error);
    connect(&decoder, &WfmDecoder::progress, this, &DecodeThread::progressChanged);

    WfmFileData* data = new WfmFileData;

    bool ok = decoder.decodeFile(data);

    file.close();

    if(ok) emit dataDecoded(QSharedPointer<WfmFileData>(data));
    else delete data;
}
