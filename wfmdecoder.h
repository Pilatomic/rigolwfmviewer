#ifndef WFMDECODER_H
#define WFMDECODER_H

#include <QString>
#include <QObject>

class WfmFileData;
class WfmChannelData;
class QIODevice;
class QString;

// This class should be split between version specific decoding functions and generic read* functions

class WfmDecoder: public QObject
{
    Q_OBJECT
public:
    WfmDecoder(QIODevice* wfmFile, QObject* parent = nullptr);

    bool decodeFile(WfmFileData *data);

signals:
    void error(QString errorString);
    void progress(int proggress);

private:
    QIODevice* wfmFile = nullptr;

    template<typename T> T readInt();
    bool readBool();
    float readFloat();
    QByteArray readBytes(size_t bytes);
    QString readString(size_t bytes);
    void skipToPos(qint64 pos);

    void readSamplesData(WfmFileData* data);
};

#endif // WFMDECODER_H
