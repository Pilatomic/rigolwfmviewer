#include "mainwindow.h"
#include "wfmdata.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    registerWfmFileDataPtrMetatype();
    MainWindow w;
    w.show();
    return a.exec();
}
