#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDataStream>
#include <QFile>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>

#include "decodethread.h"
#include "wfmdata.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showErrorMessage(QString error)
{
    QMessageBox::warning(this, "Error", error);
}

void MainWindow::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    QString("Select Waveform file to import"),
                                                    QString(),
                                                    QString("Waveform files (*.wfm)"));

    if(filename.isEmpty()) return;

    QProgressDialog* pd = new QProgressDialog(this);
    pd->setModal(true);
    pd->setWindowTitle("Loading file");
    pd->setAutoReset(false);

    DecodeThread *dt = new DecodeThread(filename, this);
    connect(dt, &DecodeThread::finished, dt, &DecodeThread::deleteLater);
    connect(dt, &DecodeThread::finished, pd, &QProgressDialog::deleteLater);
    connect(dt, &DecodeThread::progressChanged, pd, &QProgressDialog::setValue);
    connect(pd, &QProgressDialog::canceled, dt, &DecodeThread::terminate);
    connect(dt, &DecodeThread::error, this, &MainWindow::showErrorMessage);
    connect(dt, &DecodeThread::dataDecoded, ui->traceWidget, &TraceViewerWidget::setData);

    dt->start();
    pd->show();
}
