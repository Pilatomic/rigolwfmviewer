#include "wfmdecoder.h"

#include <QFile>
#include <QDebug>
#include <QElapsedTimer>

#include <iostream>
#include <exception>

#include "wfmdata.h"

#define CHANNELS_COUNT  4

static inline bool isPowerOfTwo(int n)
{
   return ((n & (n - 1)) == 0);
}

class WfmEofException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Premature end of file reached";
    }
} wfmEofEx;

class InvalidException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Invalid value";
    }
} wfmInvalidEx;


WfmDecoder::WfmDecoder(QIODevice *wfmFile, QObject *parent) :
    QObject(parent), wfmFile(wfmFile)
{

}

bool WfmDecoder::decodeFile(WfmFileData *data)
{
    Q_ASSERT(data != nullptr);
    data->isValid = false;

    QElapsedTimer decodeElapsedTimer;
    decodeElapsedTimer.start();

    emit progress(0);

    try
    {
        // Magic number header
        quint64 magicHeader = readInt<quint64>();
        if(magicHeader != 0x0038A5A6FFFFFF01) throw wfmInvalidEx;

        // Devices infos
        data->deviceModel = readString(20);
        data->deviceFirmware = readString(20);

        // Horizontal infos
        // 4 bytes of padding here to align scaleD at offset 64
        skipToPos(64);
        data->scaleD = readInt<qint64>();
        data->timeDelay = readInt<qint64>();

        //Sampling rate
        skipToPos(120);
        data->samplingRateHz = readFloat() * 1e9;

        for(int i = 0 ; i < CHANNELS_COUNT ; i++) data->channelsData.append(new WfmChannelData);

        // Channel data headers
        for(int i = 0 ; i < CHANNELS_COUNT ; i++)
        {
            readBool();     // Enabled
            readBytes(7);
            readFloat();    // Scale
            readFloat();    // Offset
            readBool();     // Inverted
            readBytes(11);
        }

        //Unknown field,
        // 2 bytes of padding here
        skipToPos(288);
        readBytes(12);      // Unknown data
        readBytes(4);       // Padding
        readBytes(39);      // More unknown data, first 3 bytes appears to change in consecutive files

        skipToPos(442);
        readInt<qint64>();  // Unknown field
        readBytes(2);       // Padding
        readInt<qint64>();  // Unknown field
        readBytes(4);       // Padding

        skipToPos(2032);
        readInt<qint64>(); // Unknown value, changes in consecutive waveforms
        readInt<qint64>(); // Unknown value

        //Extended Channel Data
        for(int i = 0; i < CHANNELS_COUNT ; i++)
        {
            WfmChannelData* channelData = data->channelsData.at(i);
            channelData->channelId          = readInt<quint8>();
            channelData->unknownField1      = readBytes(4);
            channelData->enabledExtended    = readBool();
            channelData->unknownField2      = readBytes(7);
            channelData->invertedExtended   = readBool();
            channelData->unknownField3      = readBytes(9);
            channelData->rangeId            = readInt<quint8>();
            channelData->scaleExtended      = readInt<qint64>();
            channelData->offsetExtended     = readInt<qint64>();
            channelData->unknownField4      = readBytes(8); // is always filled with 0 in my files
            channelData->label              = readString(4);
            channelData->unknownField5      = readBytes(8); // is always filled with 0 in my files
        }

        // ACQUISITION SETTINGS
        skipToPos(2317);
        for(int i = 0 ; i < CHANNELS_COUNT ; i++) readInt<quint32>();   // Another scale for each channel (same value as in extended channel header)
        for(int i = 0 ; i < CHANNELS_COUNT ; i++) readInt<qint64>();    // Another scale for each channel (same value as in extended channel header)
        for(int i = 0 ; i < CHANNELS_COUNT ; i++) readInt<quint32>();   // Active flag for each channel

        readBytes(44);
        readBytes(4);       // Channel interleave order ??

        readBytes(184);
        data->sampleCount = readInt<quint64>();

        // ACQUISITION SETTINGS DUPLICATE ( somes places have a 0x00 instead of a 0x01 )
        skipToPos(2765);
        readBytes(448);

        // SAMPLES DATA
        readSamplesData(data);

        data->isValid = true;
    }
    catch (std::exception &e) {
        emit error(QString("Decoding error : ") + QString::fromLocal8Bit(e.what()));
        return false;
    }

    qDebug() << "Decode duration :" << decodeElapsedTimer.elapsed() << "ms";
    return true;
}

void WfmDecoder::readSamplesData(WfmFileData *data)
{
    // Samples are stored as interleaved, prepare pointer array to deinterleave
    int storedChannelsCount = 0;
    quint8 *samplesDestPtr[CHANNELS_COUNT];

    const QVector<WfmChannelData*> enabledChannels = data->getEnabledChannels();


    // Need to account for the 3 channels case, in that case file is the same as with 4 channels
    // ( to keep everything 32-bit aligned I guess )
    // It is probably cleaner to round value to next power of 2 as done here rather only implemented the 3 channel case
    // ( that way is does also work for 8 channel scopes. Does Rigol sells some ? No idea ! )
    while(!isPowerOfTwo(enabledChannels.count() + storedChannelsCount))  samplesDestPtr[storedChannelsCount++] = nullptr;

    foreach(WfmChannelData* channel, enabledChannels)
    {
        channel->samplesData.resize(data->sampleCount);
        samplesDestPtr[storedChannelsCount] = channel->samplesData.data();
        storedChannelsCount++;
    }

    if(storedChannelsCount > 0)
    {
        const quint64 samplesForHalfPercent = data->sampleCount / 200;
        quint64 percentProgress = samplesForHalfPercent;
        for(quint64 i = 0 ; i < data->sampleCount ; i++)
        {
            // Copy samples value to proper channel
            for(quint8 **destPtr = samplesDestPtr ; destPtr != &samplesDestPtr[4] ; destPtr++)
            {
                if(*destPtr)
                {
                    (**destPtr) = readInt<quint8>();
                    (*destPtr)++;
                }
            }

            // Keep track of progress
            percentProgress--;
            if(percentProgress == 0)
            {
                percentProgress = samplesForHalfPercent;
                emit progress(i * 100 / data->sampleCount);
            }
        }
    }
}

QByteArray WfmDecoder::readBytes(size_t bytes)
{
    QByteArray ba;
    while(bytes > 0)
    {
        char c;
        if(!wfmFile->getChar(&c)) throw wfmEofEx;
        ba.append(c);
        bytes--;
    }
    return ba;
}

QString WfmDecoder::readString(size_t bytes)
{
    return QString::fromUtf8(readBytes(bytes));
}

void WfmDecoder::skipToPos(qint64 pos)
{
    qint64 currPos = wfmFile->pos();
    if(pos == currPos) qWarning() << "Skipping to same position" << pos;
    else if(pos < currPos) qWarning() << "Skipping to previous position" << pos;
    else qDebug() << "Skipping" << (pos-currPos) << "bytes to reach pos" << pos;
    wfmFile->seek(pos);
}

float WfmDecoder::readFloat()
{
    int i = readInt<quint32>();
    return *reinterpret_cast<float*>(&i);
}

template<typename T>
T WfmDecoder::readInt()
{
    T value = 0;
    for(uint i = 0 ; i < sizeof(T) ; i++)
    {
        uint8_t c;
        if(!wfmFile->getChar((char*)(&c))) throw wfmEofEx;
        value |= (uint64_t(c) << (8*i));
    }
    return value;
}

bool WfmDecoder::readBool()
{
    uint8_t b;
    if(!wfmFile->getChar((char*)(&b))) throw wfmEofEx;
    return b;
}
