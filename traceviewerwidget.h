#ifndef TRACEVIEWERWIDGET_H
#define TRACEVIEWERWIDGET_H

#include <QWidget>
#include <QVector>

#include "wfmdata.h"

class QPainter;

class TraceViewerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TraceViewerWidget(QWidget *parent = nullptr);

    void setData(WfmFileDataPtr data);
signals:

protected:
    void paintEvent(QPaintEvent *event);

    void paintChannelTrace(QPainter* painter, const WfmChannelData * channel);
    void paintChannelOriginIndicator(QPainter* painter, const WfmChannelData * channel);
    void paintBackGround(QPainter* painter);
    int mapValueToHeight(int value);

    WfmFileDataPtr data;
    static const QVector<QColor> tracesColor;
    static const QPolygon channelZeroPolygon;
};

#endif // TRACEVIEWERWIDGET_H
