#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "wfmdata.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();

private:
    void showErrorMessage(QString error);
    void onDataDecodingCompleted(WfmFileDataPtr decodedDataPtr);
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
