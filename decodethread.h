#ifndef DECODETHREAD_H
#define DECODETHREAD_H

#include <QThread>
#include <QFile>

#include "wfmdata.h"

class DecodeThread : public QThread
{
    Q_OBJECT
public:
    DecodeThread(const QString & path, QObject* parent = nullptr);

signals:
    void error(QString errorString);
    void dataDecoded(WfmFileDataPtr data);
    void progressChanged(int i);

    // QThread interface
protected:
    void run() override;

private:
    QString path;
    QFile file;
};

#endif // DECODETHREAD_H
