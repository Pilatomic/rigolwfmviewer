#ifndef WFMDATA_H
#define WFMDATA_H

#include <QString>
#include <QVector>
#include <QSharedPointer>

class WfmChannelData
{
public:
    WfmChannelData();

#ifdef WFM_KEEP_DUPLICATE_DATA
    bool enabledSimple;
    bool invertedSimple;
    float scaleSimple;
    float offsetSimple;

    qint64 offsetLast;
    quint32 scaleLast;
#endif
    quint8 channelId;
    quint8 rangeId;

    bool enabledExtended;
    bool invertedExtended;
    qint64 offsetExtended;
    qint64 scaleExtended; // in nV

    QByteArray unknownField1;
    QByteArray unknownField2;
    QByteArray unknownField3;
    QByteArray unknownField4;
    QByteArray unknownField5;

    QString label;

    QVector<quint8> samplesData;
};

class WfmFileData
{
public:
    WfmFileData();
    ~WfmFileData();

    QVector<WfmChannelData*> getEnabledChannels();

    bool isValid = false;
    QString deviceModel;
    QString deviceFirmware;

    qint64 scaleD;
    qint64 timeDelay;
    float samplingRateHz;

    quint64 sampleCount;

    QVector<WfmChannelData*> channelsData;
};

typedef QSharedPointer<WfmFileData> WfmFileDataPtr;
Q_DECLARE_METATYPE(WfmFileDataPtr);

void registerWfmFileDataPtrMetatype();

#endif // WFMFILEDATA_H
