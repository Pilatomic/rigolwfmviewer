#include "wfmdata.h"

WfmChannelData::WfmChannelData()
{

}

WfmFileData::WfmFileData()
{

}

WfmFileData::~WfmFileData()
{
    qDeleteAll(channelsData);
}

QVector<WfmChannelData *> WfmFileData::getEnabledChannels()
{
    QVector<WfmChannelData*> enabledChannels;
    for(auto it = channelsData.begin() ; it != channelsData.cend() ; it++)
    {
        if((*it) && (*it)->enabledExtended) enabledChannels.append(*it);
    }
    return enabledChannels;
}

void registerWfmFileDataPtrMetatype()
{
    qRegisterMetaType<WfmFileDataPtr>("WfmFileDataPtr");
}
